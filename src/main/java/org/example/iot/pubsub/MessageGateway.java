package org.example.iot.pubsub;

import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(defaultRequestChannel = "processRequestChannel")
public interface MessageGateway {
    void processReadings(final String urlEquipParam);
}
