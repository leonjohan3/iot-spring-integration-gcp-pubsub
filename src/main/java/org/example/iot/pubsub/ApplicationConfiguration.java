package org.example.iot.pubsub;

import java.net.URL;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "isigp", ignoreUnknownFields = false)
@Validated
public class ApplicationConfiguration {

    @Min(5_000)
    @Max(600_000)
    private int processReadingsScheduledRateMs;

    @Min(5_000)
    @Max(600_000)
    private int processReadingsScheduledInitialDelayMs;

    @Min(1_000)
    @Max(600_000)
    private int retryInitialBackoffIntervalMs;

    @Min(1_000)
    @Max(60_000)
    private int pubSubPublishTimeoutMs;

    @NotNull
    private URL haystackAggregatorUrl;

    @NotEmpty
    private String haystackAggregatorPathinfo;

    @NotEmpty
    private String queueOrTopicDestination;

    private boolean shouldUsePubsub;
}
