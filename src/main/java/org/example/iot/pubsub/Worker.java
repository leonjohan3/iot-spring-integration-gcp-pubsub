package org.example.iot.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
class Worker {

    @Autowired
    @Qualifier("processRequestChannel")
    private MessageChannel messageChannel;

    @Autowired
    private MessageGateway messageGateway;

    public void processReadings() {
        log.info("starting {}", Thread.currentThread().getStackTrace()[1].getMethodName());
        messageGateway.processReadings(Application.URL_EQUIP_PARAM);
        log.info("finished {}", Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
