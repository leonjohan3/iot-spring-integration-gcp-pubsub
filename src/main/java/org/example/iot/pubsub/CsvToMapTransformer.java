package org.example.iot.pubsub;

import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.integration.history.MessageHistory;
import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.opencsv.CSVReaderHeaderAware;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class CsvToMapTransformer extends AbstractTransformer {

    private boolean shouldAddAdditionalAttributes = false;

    @Override
    protected Object doTransform(final Message<?> message) throws Exception {
        Assert.notNull(message, "message must not be null");
        final Map<String, String> result = new HashMap<>();

        if (shouldAddAdditionalAttributes) {
            result.put(MessageHistory.TIMESTAMP_PROPERTY, String.valueOf(message.getHeaders().getTimestamp()));
            result.put("uuid", message.getHeaders().get("id", UUID.class).toString());
        }

        try (CSVReaderHeaderAware reader = new CSVReaderHeaderAware(new StringReader((String) message.getPayload()))) {
            result.putAll(reader.readMap());
        }
        return Collections.unmodifiableMap(result);
    }
}
