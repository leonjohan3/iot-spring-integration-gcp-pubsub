package org.example.iot.pubsub;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.integration.transformer.AbstractPayloadTransformer;
import org.springframework.util.Assert;

import com.opencsv.CSVReaderHeaderAware;

public class CsvToListOfMapsTransformer extends AbstractPayloadTransformer<String, List<Map<String, String>>> {

    @Override
    protected List<Map<String, String>> transformPayload(final String payload) throws Exception {
        Assert.hasLength(payload, "the payload must not be null or empty");
        final List<Map<String, String>> result = new ArrayList<>();

        try (CSVReaderHeaderAware reader = new CSVReaderHeaderAware(new StringReader(payload))) {

            Map<String, String> item;

            while (null != (item = reader.readMap())) {
                result.add(Collections.unmodifiableMap(item));
            }
        }
        return Collections.unmodifiableList(result);
    }
}
