package org.example.iot.pubsub;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.aopalliance.aop.Advice;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.JmsProperties.AcknowledgeMode;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.integration.json.ObjectToJsonTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.Assert;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableIntegration
@EnableScheduling
@SpringBootApplication
@EnableConfigurationProperties(ApplicationConfiguration.class)
@IntegrationComponentScan
public class Application {

    static final String URL_EQUIP_PARAM = "?filter=equipRef";
    static final MediaType MEDIA_TYPE_TEXT_CSV = new MediaType(MediaType.TEXT_PLAIN.getType(), "csv");

    private static final String DEVICE_ID_HEADER_NAME = "deviceId";
    private static final String LOG_CATEGORY_NAME = "org.example.iot.pubsub";
    private static final String START_TIMESTAMP_HEADER_NAME = "startTimestamp";

    @SuppressWarnings("serial")
    static final String MEDIA_TYPE_TEXT_CSV_VALUE = MediaType.toString(new ArrayList<MediaType>() {{ add(MEDIA_TYPE_TEXT_CSV); }});

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private ApplicationConfiguration configuration;

    @Autowired
    private Environment environment;

    @Autowired
    private Worker worker;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private PubSubTemplate pubsubTemplate;

    private final AtomicLong endToEndElapsedMs = new AtomicLong(0);

    @Bean
    public PooledConnectionFactory connectionFactory() {
        return new PooledConnectionFactory(environment.getProperty("spring.activemq.broker-url"));
    }

    @Bean
    public MessageChannel processRequestChannel() {
        return MessageChannels.direct().datatype(String.class).get();
    }

    @Bean
    public Gauge endToEndElapsedMsGauge(final MeterRegistry meterRegistry) {
        Assert.notNull(meterRegistry, "meterRegistry must not be null");
        return Gauge.builder("end.to.end.elapsed.time", endToEndElapsedMs, value -> value.get()).register(meterRegistry);
    }

    @Bean
    public Advice retryAdvice() {
       final RequestHandlerRetryAdvice advice = new RequestHandlerRetryAdvice();
       final RetryTemplate retryTemplate = new RetryTemplate();
       final ExponentialBackOffPolicy exponentialBackOffPolicy = new ExponentialBackOffPolicy();
       exponentialBackOffPolicy.setInitialInterval(configuration.getRetryInitialBackoffIntervalMs());
       retryTemplate.setBackOffPolicy(exponentialBackOffPolicy);
       advice.setRetryTemplate(retryTemplate);
       return advice;
    }

    @Bean
    public MessageHandler pubSubMessageSender() {
        final PubSubMessageHandler pubSubMessageHandler = new PubSubMessageHandler(pubsubTemplate, configuration.getQueueOrTopicDestination());
        pubSubMessageHandler.setSync(true);
        pubSubMessageHandler.setPublishTimeout(configuration.getPubSubPublishTimeoutMs());
        return pubSubMessageHandler;
    }

    @Bean
    public MessageChannel executorChannel() {
        return MessageChannels.executor(taskExecutor).datatype(Map.class).get();
    }

    @SuppressWarnings("unchecked")
    @Bean
    public IntegrationFlow processReadings() {
        return f -> f.channel(processRequestChannel())
                        .enrichHeaders(s -> s.header(HttpHeaders.ACCEPT, MEDIA_TYPE_TEXT_CSV)
                                             .headerFunction(START_TIMESTAMP_HEADER_NAME, m -> m.getHeaders().getTimestamp()))
                        .handle(Http.outboundGateway(configuration.getHaystackAggregatorUrl().toString() + "/"
                                                                        + configuration.getHaystackAggregatorPathinfo() + Application.URL_EQUIP_PARAM)
                                        .httpMethod(HttpMethod.GET)
                                        .expectedResponseType(String.class)
                                        , e -> e.advice(retryAdvice()))
                        .headerFilter(HttpHeaders.TRANSFER_ENCODING, HttpHeaders.ACCEPT, "Server", "contentType", "http_statusCode")
                        .transform(new CsvToListOfMapsTransformer())
                        .log(LoggingHandler.Level.INFO, LOG_CATEGORY_NAME, m -> "number of devices: " + ((List<Map<String, String>>) m.getPayload()).size())
                        .split()
                        .channel(executorChannel());
    }

    @Bean
    public IntegrationFlow processEachDevice() {
        return IntegrationFlows.from(executorChannel())
                        .route(Map.class, p -> Boolean.valueOf((String) p.get("active")), cm -> cm.defaultOutputToParentFlow()
                                        .resolutionRequired(false)
                                        .channelMapping(false, "aggregate.input"))
                        .transform(Map.class, p -> p.get("point"))
                        .enrichHeaders(s -> s.header(HttpHeaders.ACCEPT, MEDIA_TYPE_TEXT_CSV))
                        .handle(Http.outboundGateway(configuration.getHaystackAggregatorUrl().toString() + "/"
                                                              + configuration.getHaystackAggregatorPathinfo() + "?id={" + DEVICE_ID_HEADER_NAME + "}")
                                        .httpMethod(HttpMethod.GET)
                                        .uriVariable(DEVICE_ID_HEADER_NAME, m -> m.getPayload())
                                        .expectedResponseType(String.class)
                                        , e -> e.advice(retryAdvice()))
                        .headerFilter(HttpHeaders.TRANSFER_ENCODING, HttpHeaders.ACCEPT, "Server", "contentType", "http_statusCode")
                        .transform(new CsvToMapTransformer(true))
                        .transform(new ObjectToJsonTransformer())
                        .headerFilter("json__ContentTypeId__", "json__TypeId__", "json__KeyTypeId__", "contentType")
                        .log(LoggingHandler.Level.DEBUG, LOG_CATEGORY_NAME)
                        .wireTap(wf -> wf.route(Message.class, p -> configuration.isShouldUsePubsub(), cm -> cm.defaultOutputToParentFlow()
                                                        .resolutionRequired(false)
                                                        .subFlowMapping(true, sf -> sf.handle(pubSubMessageSender())))
                                         .handle(Jms.outboundAdapter(connectionFactory())
                                                         .destination(configuration.getQueueOrTopicDestination())
                                                         .configureJmsTemplate(t -> t.sessionAcknowledgeMode(AcknowledgeMode.AUTO.getMode())
                                                                         .sessionTransacted(false))))
                        .transform(Message.class, m -> new String())
                        .channel("aggregate.input")
                        .get();
    }

    @Bean
    public IntegrationFlow aggregate() {
        return f -> f.aggregate()
                     .handle((p, h) -> endToEndElapsedMs.getAndSet(h.getTimestamp() - h.get(START_TIMESTAMP_HEADER_NAME, Long.class)))
                     .channel(IntegrationContextUtils.NULL_CHANNEL_BEAN_NAME);
    }

    // Note: a default single threaded TaskScheduler is created by the @EnableScheduling annotation, so a new task/thread will not
    // start (until the current task/thread has completed. If the TaskScheduler is multi-threaded, this would happen, and that could be a
    // problem, and for the sake of simplicity, we would rather avoid that scenario. See docs on @EnableScheduling
    @Scheduled(fixedRateString = "${isigp.process-readings-scheduled-rate-ms}", initialDelayString = "${isigp.process-readings-scheduled-initial-delay-ms}")
    public void process() {

        try {
            worker.processReadings();
        } catch (final Exception e) {
          log.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
        }
    }
}
