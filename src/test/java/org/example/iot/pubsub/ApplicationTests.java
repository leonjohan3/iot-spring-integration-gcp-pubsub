package org.example.iot.pubsub;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE, properties = {
         "isigp.haystack-aggregator-url=http://localhost:${wiremock.server.port}",
         "isigp.process-readings-scheduled-initial-delay-ms=600000",
         "isigp.should-use-pubsub=false",
         "spring.activemq.broker-url=vm://localhost?broker.persistent=false&broker.useJmx=false" })
@AutoConfigureWireMock(port = 0)
public class ApplicationTests {

    private static final String RESOURCE_BASE_LOC = "org/example/iot/pubsub/";
    private static final String DEVICE_A_URI_PARAM = "?id=@S.ORG.item_POC.Z1_CO2";
    private static final String DEVICE_B_URI_PARAM = "?id=@S.ORG.item_POC.Z1_TEMP";

    @Autowired
    private ApplicationConfiguration configuration;

    private JmsTemplate jmsTemplate;

    @Autowired
    public void setConnectionFactory(final PooledConnectionFactory pooledConnectionFactory) {
        jmsTemplate = new JmsTemplate(pooledConnectionFactory);
    }

    @Autowired
    private Worker worker;

    @Test
    public void shouldGetReadingsAndPlaceOnQueueOrTopic() {

        // given: all data (test fixture) preparation
        givenThat(get(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + Application.URL_EQUIP_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE))
                        .willReturn(aResponse().withHeader(HttpHeaders.CONTENT_TYPE, Application.MEDIA_TYPE_TEXT_CSV_VALUE)
                        .withBodyFile(RESOURCE_BASE_LOC + "EquipmentList.csv")));

        givenThat(get(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + DEVICE_A_URI_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE))
                        .willReturn(aResponse().withHeader(HttpHeaders.CONTENT_TYPE, Application.MEDIA_TYPE_TEXT_CSV_VALUE)
                        .withBodyFile(RESOURCE_BASE_LOC + "DeviceA.csv")));

        givenThat(get(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + DEVICE_B_URI_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE))
                        .willReturn(aResponse().withHeader(HttpHeaders.CONTENT_TYPE, Application.MEDIA_TYPE_TEXT_CSV_VALUE)
                        .withBodyFile(RESOURCE_BASE_LOC + "DeviceB.csv")));

        // when : method to be checked invocation
        worker.processReadings();

        // then : checks and assertions
        jmsTemplate.setReceiveTimeout(1_000);
        final List<String> results = new ArrayList<>();
        String result;

        while (null != (result = (String) jmsTemplate.receiveAndConvert(configuration.getQueueOrTopicDestination()))) {
            results.add(result);
        }

        verify(getRequestedFor(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + Application.URL_EQUIP_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE)));

        verify(getRequestedFor(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + DEVICE_A_URI_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE)));

        verify(getRequestedFor(urlEqualTo("/" + configuration.getHaystackAggregatorPathinfo() + DEVICE_B_URI_PARAM))
                        .withHeader(HttpHeaders.ACCEPT, equalTo(Application.MEDIA_TYPE_TEXT_CSV_VALUE)));

        assertThat(results, hasSize(2));
        assertThat(results.get(0), anyOf(containsString("\"curVal\":\"n:21.34 degc\""), containsString("\"curVal\":\"n:550.043 ppm\"")));
    }
}
