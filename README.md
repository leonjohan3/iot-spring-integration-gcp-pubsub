# Overview
This project demonstrates one way to implement functionality to pull [IoT](<https://en.wikipedia.org/wiki/Internet_of_things>) readings from a [time series database](<https://en.wikipedia.org/wiki/Time_series_database>) that exposes IoT devices using [RESTful web services](<https://en.wikipedia.org/wiki/Representational_state_transfer>) and push these readings to a [messaging system](<https://en.wikipedia.org/wiki/Message-oriented_middleware>) for further processing. The RESTful service used as the source of the data, is modeled on the open source [Project Haystack](<https://project-haystack.org>).

The aim is to deploy the application in a [k8s](<https://en.wikipedia.org/wiki/Kubernetes>) cluster in one of the [cloud](<https://en.wikipedia.org/wiki/Cloud_computing>) providers. For local testing either [ActiveMQ](<https://en.wikipedia.org/wiki/Apache_ActiveMQ>) or the [GCP PubSub emulator](<https://cloud.google.com/pubsub/docs/emulator>) can be used.

This project also shows how to expose metrics using [Spring Prometheus support](<https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/reference/html/production-ready-metrics.html#production-ready-metrics-export-prometheus>) used together with the [GKE Stackdriver Prometheus](<https://cloud.google.com/monitoring/kubernetes-engine/prometheus>) integration support with the [Stackdriver Prometheus sidecar](<https://github.com/Stackdriver/stackdriver-prometheus-sidecar/blob/master/README.md>). Simply enabling the Prometheus feature in the Spring Boot Actuator and using the sidecar ensures that the code is not tied to a specific cloud vendor. GCP Stackdriver external metrics incurs high charges. Before enabling this, ensure you understand the [pricing](<https://cloud.google.com/stackdriver/pricing#monitoring-costs>) and create billing budget alerts.    

# Process
In Summary: loop continuously reading the device aggregator's devices' readings, publishing the data retrieved to the messaging middleware.

1.  Get a list of devices from the aggregator in CSV format.
2.  For each device, call the REST endpoint to get the current reading (also in CSV format). Do step 2 to 6 using multiple threads.
3.  Convert the CSV to a Map.
4.  Tag the reading with the local time ([seconds since the Epoch](<https://en.wikipedia.org/wiki/Unix_time>)) and a [UUID](<https://en.wikipedia.org/wiki/Universally_unique_identifier>) (to identify duplicates).
5.  Convert the Map to a JSON object.
6.  Publish the JSON object to the messaging middleware queue/topic.

# Reference Documentation
* [Official Apache Maven documentation](<https://maven.apache.org/guides/index.html>)
* [Spring Boot Maven Plugin Reference Guide](<https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/maven-plugin/>)
* [Spring Configuration Processor](<https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/reference/htmlsingle/#configuration-metadata-annotation-processor>)
* [Spring Integration](<https://docs.spring.io/spring-integration/docs/5.2.0.RELEASE/reference/html>)
* [Spring Integration Testing](<https://docs.spring.io/spring-integration/docs/5.2.0.BUILD-SNAPSHOT/reference/html/testing.html#test-context>)
* [Spring for Apache ActiveMQ Artemis](<https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/reference/htmlsingle/#boot-features-artemis>)
* [GCP Messaging](<https://docs.spring.io/spring-cloud-gcp/docs/1.1.0.M3/reference/htmlsingle/#_spring_cloud_gcp_for_pub_sub>)
* [Google Cloud Pub/Sub](<https://cloud.spring.io/spring-cloud-gcp/reference/html/#google-cloud-pubsub>)
* [Spring Integration Cloud GCP](<https://docs.spring.io/spring-cloud-gcp/docs/1.1.0.M1/reference/html/_spring_integration.html>)
* [Setting Up Stackdriver Logging for Java](<https://cloud.google.com/logging/docs/setup/java>)
* [Logback Configuration](<https://logback.qos.ch/manual/configuration.html>)
* [Haystack REST API used as an example IoT aggregator](<https://project-haystack.org/doc/Rest>)
* [Wiremock Standalone](<http://wiremock.org/docs/running-standalone/>)
* [GCP Client Libraries Auth](<https://cloud.google.com/pubsub/docs/reference/libraries#client-libraries-install-java>)
* [Deploy a Java application to GKE](<https://codelabs.developers.google.com/codelabs/cloud-springboot-kubernetes/#4>)
* [Deploy a non-Java image to GKE](<https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app>)
* [Testing apps locally with the GCP PubSub emulator](<https://cloud.google.com/pubsub/docs/emulator>)
* [Creating and enabling GCP service accounts](<https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances>)

# Samples
* [GCP Pub/Sub Sample](<https://github.com/spring-cloud/spring-cloud-gcp/tree/master/spring-cloud-gcp-samples/spring-cloud-gcp-pubsub-sample>)

# Running Wiremock Standalone (for local testing) 
    java -jar wiremock-standalone-2.25.0.jar --port 9999 --bind-address localhost

# Update gcloud
    gcloud components update
    gcloud components install COMPONENT_ID
    
# Build and deploy the Java application from the project folder
    ./mvnw -DskipTests clean package
    ./mvnw -DskipTests com.google.cloud.tools:jib-maven-plugin:build -Dimage=gcr.io/my-project/iot-java:v1
    kubectl apply -f iot-java-deploy.yaml
    kubectl apply -f iot-java-service.yaml
    
# To use a GCP credentials encoded key in the application.yaml
    base64 downloaded-credentials.json -w 0 > /tmp/output-for-encoded-key.txt 

# Tags
Java, Maven, Spring Boot, Spring Integration, JMS, ActiveMQ, Wiremock, Project Haystack, GCP PubSub, WebFlux, [Markdown](https://en.wikipedia.org/wiki/Markdown).
